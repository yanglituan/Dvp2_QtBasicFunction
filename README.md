### 想要同一套代码在不同平台正常编译运行，要做好以下事情：

#### 1、下载对应相机的驱动包，安装驱动包

您可以前往[度申科技开发者中心](http://112.124.21.30/download/Driver/)下载最新的驱动

```
GX/      /* 对应GX系列 */
Linux/   /* Dvp Linux平台驱动（Arm64、Arm32、Linux x86 64位、Linux x86 32位）*/
M2S/     /* 对应M2S系列 */
M3S/     /* 对应M3S系列 */
MGS/     /* 对应MGS系列 */
```

Linux arm平台和Linux x86驱动安装是一样的，步骤如下：

```
1、安装：连接相机后，进入驱动包，输入命令  sudo ./install.sh, 可以选择重启系统
2、sdk目录介绍：
------------------------------
-- basedcam2                   /* DVP2相机演示程序 */
-- Demo_x64                    /* 可执行程序demo,无界面 */
-- libdvp.so                   /* sdk api库 */
-- libhzd.so                   /* 核心算法库 */
-- usb2_m_all.dscam.so         /* usb2相机设备驱动库 */
-- usb3_m_all.dscam.so         /* U3S系列相机设备驱动库 */
-- usb3_v_all.dscam.so         /* U3V系列相机设备驱动库 */
-- usb3_m3s_all.dscam.so       /* M3S系列相机设备驱动库 */
-- BasicFuncation.tar.gz       /* qt示例源码
注意事项：
libdvp.so  libhzd.so usbxxx.dscam.so 库必须放在同一级目录 
```

Windows电脑上直接打开exe，按照提示安装即可。

#### 2、配置对应平台的引用库文件

进入目录，使用Qt打开.pro文件，头文件是DVPCamera.h，Linux下dvplib.so对应Windows上的DVPCamera.dll文件。可以参考下面QtBasicFunction在Linux x86虚拟机上的配置方式。

在Linux x86上编译需要在pro文件中修改库目录的配置：

```
unix:!macx: LIBS += -L$$PWD/lib_linux_x86_64/ -ldvp

INCLUDEPATH += $$PWD/lib_linux_x86_64
DEPENDPATH += $$PWD/lib_linux_x86_64
```

在Linux arm上编译需要在pro文件中修改库目录的配置：

```
unix:!macx: LIBS += -L$$PWD/lib_linux_arm64/ -ldvp

INCLUDEPATH += $$PWD/lib_linux_arm64
DEPENDPATH += $$PWD/lib_linux_arm64
```

Windows配置类似。

#### 3、编译运行

Windows：在Qt Creator中进行编译即可

Linux x86：安装好Qt Creator、编译器等Qt环境编译即可编译

Linux Arm有两种编译方式：

- 交叉编译：需要在虚拟机中安装好交叉编译器以及Arm Qt库再编译
- Arm直接编译：比如RK3399、Nvidia TX2平台，在RK3399、Nvidia TX2上装好Qt环境(`sudo apt install qt5-default qtcreator`)后，是能在ARM上编译、运行和调试的

编译完成后，把驱动安装包下的`有关的.so库复制到可执行文件BasicFunction的同级目录下`

```
build-BasicFunction-Desktop-Debug$ tree 
. 
├── BasicFunction
├── BasicFunction.o
├── ImageAcquisition.o
├── libD3tKit.so			//	√
├── libdvp.so               //对应Win10平台的DVPCamera.dll，相机API库  √
├── libhzd.so               //一些图像处理库  √
├── main.o
├── Makefile
├── moc_BasicFunction.cpp
├── moc_BasicFunction.o
├── moc_ImageAcquisition.cpp
├── moc_ImageAcquisition.o
├── ui_BasicFunction.h
├── usb2_m_all.dscam.so       //usb2.0驱动      √
├── usb3_m3s_all.dscam.so     //m3s系列相机驱动   √
├── usb3_m_all.dscam.so       //usb3系列驱动     √
└── usb3_v_all.dscam.so       //usb3系列驱动	 √
```
### Linux x86虚拟机运行截图如下：

![avatar](./Image/BasicFunction.png)


### Win x86运行截图

![Win x86](./Image/BasicFunction_x86.png)

### Linux Arm（RK3399）运行截图如下

![linux arm64](./Image/Linux_arm64_rk3399.png)

### 4、其他
采集的图片要想显示在Qt控件上显示要转成BGR24,其他目标格式这里暂时没有做太多处理，BasicFunction设置相机的目标格式为BGR24,
如果不是BGR24，比如MONO格式，那需要自己转成能显示的RGB格式，目前示例程序没有做这些处理，只显示目标格式为BGR24。
```
//ImageAcquisition.cpp
dvpStatus status;
status = dvpGetFrame(m_handle, &m_pFrame, &pBuffer, m_uGrabTimeout);

if (status == DVP_STATUS_OK)
{
	//这里将采集图像、图像转换放置在工作线程中实现，解决主界面在高帧率显示时卡顿问题
	if(m_pFrame.format==FORMAT_BGR24)
	{
		m_threadMutex.lock();

#if (QT_VERSION >= QT_VERSION_CHECK(5,14,0))
		// Qt5.14版本新增QImage::Format_BGR888类型
		m_ShowImage = QPixmap::fromImage(QImage((uchar*)pBuffer,m_pFrame.iWidth, m_pFrame.iHeight,m_pFrame.iWidth*3, QImage::Format_BGR888,0,0)); // 5.13
#else
		//其他版本先把BGR数据转成RGB数据，再用RGB数据转QImage
		BGR2RGB((uchar*)pBuffer,m_pFrame.iWidth, m_pFrame.iHeight);
		m_ShowImage = QPixmap::fromImage(QImage((uchar*)pBuffer,m_pFrame.iWidth, m_pFrame.iHeight,m_pFrame.iWidth*3, QImage::Format_RGB888,0,0)); //5.9
#endif
		m_threadMutex.unlock();
		emit signalDisplay();
	}
}
```
