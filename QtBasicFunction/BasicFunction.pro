#-------------------------------------------------
#
# Project created by QtCreator 2021-03-01 10:11:40
#
#-------------------------------------------------

QT       += core gui
INCLUDEPATH += /home/QTCreatorPro/
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BasicFunction
TEMPLATE = app


QMAKE_LFLAGS += -Wl,-rpath=.

SOURCES += main.cpp\
        BasicFunction.cpp \
    ImageAcquisition.cpp \
    MyGraphicsitem.cpp

HEADERS  += BasicFunction.h \
    ImageAcquisition.h \
    DVPCamera.h \
    MyGraphicsitem.h

FORMS    += BasicFunction.ui

#RC_ICONS = BasicFunction.ico

win32{
    contains(QMAKE_HOST.arch, x86_64) {
        message("x86_64 build")
        ## Windows x64 (64bit) specific build here
        LIBS += -L$$PWD/lib_x86_64/ -lDVPCamera64
        INCLUDEPATH += $$PWD/lib_x86_64
        DEPENDPATH += $$PWD/lib_x86_64
    }else{
        message("x86_32 build")
        ## Windows x86 (32bit) specific build here
        LIBS += -L$$PWD/lib_x86_32/ -lDVPCamera
        INCLUDEPATH += $$PWD/lib_x86_32
        DEPENDPATH += $$PWD/lib_x86_32
    }
}


unix{
    #Linux平台库配置,右键项目配置库
    if(contains(QT_ARCH, x86_64)){
        message("linux_x86_64 build")
        LIBS += -L$$PWD/lib_linux_x86_64/ -ldvp

        INCLUDEPATH += $$PWD/lib_linux_x86_64
        DEPENDPATH += $$PWD/lib_linux_x86_64
    }else{
        if(contains(QT_ARCH, arm64)){
            message("linux_arm64 build")
            LIBS += -L$$PWD/lib_linux_arm64/ -ldvp

            INCLUDEPATH += $$PWD/lib_linux_arm64
            DEPENDPATH += $$PWD/lib_linux_arm64
        }else{
            contains(QT_ARCH, arm){
                message("linux_arm build")
                LIBS += -L$$PWD/lib_linux_arm32/ -ldvp

                INCLUDEPATH += $$PWD/lib_linux_arm32
                DEPENDPATH += $$PWD/lib_linux_arm32
            }
        }
    }
}

